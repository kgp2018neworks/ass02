## 1. HTTP Server

* #### Classification of ports
  - ##### PORT 8100
    HTTP 1.0
  ![alt-text](./8100.png)
  - ##### PORT 8110
    HTTP 1.1 without persistent connection
  ![alt-text](./8110.png)
  - ##### PORT 8111
    HTTP 1.1 with persistent connection
  ![alt-text](./8111.png)

  The HTTP version is present in the response header, in all these images.<br>
  Whether the connection is persistent or not is present in the 'Keep-Alive' field of the response header.

* #### Number of GET requests

| PORT | # GET requests |
|-----:|:---------------|
| 8100 | 18             |
| 8110 | 18             |
| 8111 | 18             |

* #### Time elapsed (in micro seconds)

|Page/ Port | 8100 | 8110 | 8111 |
|----------:|:----:|:----:|:----:|
|/         |1373.00 | 1374.58 | 1374.58 |
|/style.css|1015.68 | 909.49 | 909.49 |
|/mobile.css|1605.35 | 1416.86 | 1416.86 |
|mobile.js|2484.92 | 2287.61 | 2287.61 |
|/logo.png|925.77 | 973.28 | 973.28 |
|/satellite.png|4614.66 | 4476.88 | 4476.88 |
|/project-image1.jpg|1895.18 | 1339.97 | 1339.97 |
|/project-image2.jpg|1040.73 | 1618.23 | 1618.23 |
|/project-image3.jpg|1570.96 | 1610.26 | 1610.26 |
|/project-image4.jpg|1668.08 | 1583.03 | 1583.03 |
|/mars-recover.jpg|1795.90 | 1931.50 | 1931.50 |
|/finding-planet.jpg|1949.98 | 1839.07 | 1839.07 |
|/audiowide-regular-webfont.woff|1538.18 | 700.04 | 700.04 |
|/new-satelliedish.jpg|1163.50 | 677.84 | 677.84 |
|/bg-home.jpg|684.01 | 931.43 | 931.43 |
|/bg-transparent1.png|649.45 | 784.80 | 784.80 |
|/icons.png|8887.97 | 13995.91 | 13995.91 |
|/mobile-menu.png|841.17 | 851.01 | 851.01 |

* #### Total page download time (in seconds)

| PORT | Time(seconds) |
|-----:|:----------------|
| 8100 | 0.195691124     |
| 8110 | 0.512433843     |
| 8111 | 0.400352644     |

* #### http_req.user-agent
	- Value -> Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36
		(KHTML, like Gecko) Ubuntu Chromium/63.0.3239.84 Chrome/63.0.3239.84 Safari/537.36
	- Information 
		* OS -> Ubuntu 
		* Browser -> Chromium/63.0.3239.84 Chrome/63.0.3239.84 Safari/537.36
